class CreateJoinEights < ActiveRecord::Migration
  def self.up
   create_table :join_eights do |t|
     t.string  :name
     t.string  :email
     t.string  :company
     t.integer :mobile
     
    end
  end

  def self.down
    drop_table :join_eights
  end
end
