
<%= form_for(@message) do |f| %>
  <%= recaptcha_tags %>
  <div class="actions">
    <%= f.submit %>
  </div>
<% end %>
