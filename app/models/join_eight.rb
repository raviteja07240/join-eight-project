class JoinEight < ActiveRecord::Base

  validates_format_of(:email, :with => /^([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})$/, :message => "Please provide your Email in Proper Format",:if => Proc.new { |u| !u.email.blank? })

end
